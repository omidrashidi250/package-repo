#!/bin/bash

cd "${0%/*}"

name="etesync-web"
version="$(curl -s https://api.github.com/repos/etesync/etesync-web/releases/latest | grep tag_name | sed 's/.*v//' | sed 's/".*//')"
description="An EteSync web client"

mkdir "$name"

echo "pkgname=\"$name\"
pkgver=\"$version\"
arch=(\"any\")
pkgdesc=\""$description"\"
pkgrel=\"1\"
source=(\"git+https://github.com/etesync/etesync-web\")
sha256sums=(\"SKIP\")
depends=(\"yarn\")

package() {
    cd \"\${srcdir}\"/etesync-web

    sed -i -z \"s/if (process.env.NODE_ENV !== \\\"development\\\") {\\\n        if (showAdvanced && \\\!server.startsWith(\\\"https:\\\/\\\/\\\")) {\\\n          errors.server = \\\"Server URI must start with https:\\\/\\\/\\\";\\\n        }\\\n      }\\\n\\\n      //\" src/SignupPage.tsx
    sed -i -z \"s/error={\\\!\\\!errors.server}\\\n          helperText={errors.server}\\\n          //\" src/SignupPage.tsx
    sed -i -z \"s/if (process.env.NODE_ENV !== \\\"development\\\") {\\\n      if (showAdvanced && \\\!server.startsWith(\\\"https:\\\/\\\/\\\")) {\\\n        errors.errorServer = \\\"Server URI must start with https:\\\/\\\/\\\";\\\n      }\\\n    }\\\n\\\n    //\" src/components/LoginForm.tsx

    npm i
    yarn build

    yarn global add serve
    npm install serve

    install -d \"\${pkgdir}\"/opt/"$name"
    cp -r . \"\${pkgdir}\"/opt/"$name"/

    echo \"cd /opt/etesync-web
    yarn serve -s build\" | tee \"\${pkgdir}\"/opt/"$name"/"$name"
    chmod +x \"\${pkgdir}\"/opt/"$name"/"$name"
    
    install -d \"\${pkgdir}\"/usr/bin
    ln -s /opt/"$name"/"$name" \"\${pkgdir}\"/usr/bin
    chmod -R 755 \"\${pkgdir}\"/opt/"$name"
}" | tee "$name"/PKGBUILD