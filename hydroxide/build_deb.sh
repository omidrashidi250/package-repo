#!/bin/bash

if [[ -z "$maintainer_name" ]]; then
    maintainer_name="Julian Fairfax"
fi
if [[ -z "$maintainer_email" ]]; then
    maintainer_email="juliannfairfax@protonmail.com"
fi

cd "${0%/*}"

Build(){
    git clone https://github.com/emersion/hydroxide

    cd hydroxide

    if [[ $arch == "i386" ]]; then
        GOOS=linux GOARCH=386 GO111MODULE=on go build ./cmd/hydroxide
    elif [[ $arch == "armhf" ]]; then
        GOOS=linux GOARCH=arm GO111MODULE=on go build ./cmd/hydroxide
    else
        GOOS=linux GOARCH=$arch GO111MODULE=on go build ./cmd/hydroxide
    fi

    name="hydroxide"
    version="$(curl -s https://api.github.com/repos/emersion/hydroxide/releases/latest | grep tag_name | sed 's/.*v//' | sed 's/".*//')"
    description="A third-party, open-source ProtonMail CardDAV, IMAP and SMTP bridge"

    mv $name $name-bin
    install -d $name/usr/bin

    cp $name-bin $name/usr/bin/$name

    chmod +x $name/usr/bin/$name
    mkdir $name/DEBIAN

    echo "Package: $name
Version: $version
Architecture: $arch
Maintainer: $maintainer_name <$maintainer_email>
Description: $description
Section: misc
Priority: extra" | tee $name/DEBIAN/control

    dpkg-deb -Z xz -b $name/ .

    rm -r $name
    cp *.deb ../

    cd ../
    rm -rf hydroxide
}

export arch=amd64 && Build
export arch=i386 && Build
export arch=arm64 && Build
export arch=armhf && Build