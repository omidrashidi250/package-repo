#!/bin/sh

sudo -u makepkg export GPG_TTY=$(tty)

sudo -u makepkg openssl aes-256-cbc -d -in priv.gpg.enc -out priv.gpg -k "$PASSPHRASE"
sudo -u makepkg gpg2 --import pub.gpg && sudo -u makepkg gpg2 --batch --import --batch priv.gpg


git clone https://gitlab.com/julianfairfax/pi-capsule-project

chmod +x pi-capsule-project/build_arch.sh
pi-capsule-project/build_arch.sh

chown -R makepkg pi-capsule-project

cd pi-capsule-project/pi-capsule-project
sudo -u makepkg makepkg --sign

cp *.pkg.tar.zst* ../../

cd ../../

git clone https://gitlab.com/julianfairfax/repackage

chmod +x repackage/build_arch.sh
repackage/build_arch.sh

chown -R makepkg repackage

cd repackage/repackage
sudo -u makepkg makepkg --sign

cp *.pkg.tar.zst* ../../

cd ../../

git clone https://gitlab.com/julianfairfax/spoofer

chmod +x spoofer/build_arch.sh
spoofer/build_arch.sh

chown -R makepkg spoofer

cd spoofer/spoofer

sudo -u makepkg makepkg --sign

cp *.pkg.tar.zst* ../../

cd ../../

chmod +x etesync-web/build_arch.sh
etesync-web/build_arch.sh

chown -R makepkg etesync-web

cd etesync-web/etesync-web
sudo -u makepkg makepkg --sign

cp *.pkg.tar.zst* ../../

cd ../../


chmod +x gnome-dav-support/build_arch.sh
gnome-dav-support/build_arch.sh

chown -R makepkg gnome-dav-support

cd gnome-dav-support/gnome-dav-support
sudo -u makepkg makepkg --sign

cp *.pkg.tar.zst* ../../

cd ../../


export GPG_TTY=$(tty)

openssl aes-256-cbc -d -in priv.gpg.enc -out priv.gpg -k "$PASSPHRASE"
gpg2 --import pub.gpg && gpg2 --batch --import priv.gpg

repo-add package-repo.db.tar.gz *.pkg.tar.zst -s

echo -e "\e[0;32mDeploy to Gitlab Pages...\e[0m"