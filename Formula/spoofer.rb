class Spoofer < Formula
    desc "Command line tool to spoof your MAC address and scan your network"
    homepage "https://gitlab.com/julianfairfax/spoofer"
    url "https://gitlab.com/julianfairfax/spoofer/-/archive/2cb42d831c1b22a3dae0ff3e84b7bb0c24484bd2/spoofer-2cb42d831c1b22a3dae0ff3e84b7bb0c24484bd2.tar.gz"
    sha256 "14e3e81740c7ddb864a08418855e63ea04ebc88ab35f6ba24008df63d6d55a8b"
    license "GPL"
    version "2.4.5"
  
    def install
        bin.install "spoofer.sh" => "spoofer"
        prefix.install Dir["resources/*"]
    end
end