class MacosPatcher < Formula
    desc "macOS command line tool for running macOS on unsupported Macs"
    homepage "https://gitlab.com/julianfairfax/macos-patcher"
    url "https://gitlab.com/julianfairfax/macos-patcher/-/archive/5ea1067c53dd0170472a7a07a47170978fe267d1/macos-patcher-5ea1067c53dd0170472a7a07a47170978fe267d1.tar.gz"
    sha256 "a719fa9a8e61d68957e435ca68fcbfd05fde9e4340d63a06ca9ab6e6f01fb606"
    license "GPL"
    version "3.6"
  
    def install
        bin.install "macOS Patcher.sh" => "macos-patcher"
        prefix.install Dir["resources/*"]
    end
end