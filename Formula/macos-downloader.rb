class MacosDownloader < Formula
    desc "macOS command line tool for downloading macOS installers and beta updates"
    homepage "https://gitlab.com/julianfairfax/macos-downloader"
    url "https://gitlab.com/julianfairfax/macos-downloader/-/archive/e8a0e86066812a96c7c26b903b737f695eacdc9d/macos-downloader-e8a0e86066812a96c7c26b903b737f695eacdc9d.tar.gz"
    sha256 "286f09a248b78721fea750962aaa37064484487fb9c789490323b7d2094cf069"
    license "GPL"
    version "2.5"
  
    def install
        bin.install "macOS Downloader.sh" => "macos-downloader"
        bin.install "Search Catalog.sh" => "search-catalog"
        prefix.install Dir["resources/*"]
    end
end