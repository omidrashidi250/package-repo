class OsxPatcher < Formula
    desc "macOS command line tool for running OS X on unsupported Macs"
    homepage "https://gitlab.com/julianfairfax/osx-patcher"
    url "https://gitlab.com/julianfairfax/osx-patcher/-/archive/ec3c732bc13e4413672c1e3e6d3891e8618915c8/osx-patcher-ec3c732bc13e4413672c1e3e6d3891e8618915c8.tar.gz"
    sha256 "b558631aaa277044625f0d443270bfbb58a25456c662c98e9f74fb8e7b4964ec"
    license "GPL"
    version "1.2"
  
    def install
        bin.install "OS X Patcher.sh" => "osx-patcher"
        prefix.install Dir["resources/*"]
    end
end