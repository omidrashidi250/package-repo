class Openinstallmedia < Formula
    desc "The open source install media creation tool for macOS"
    homepage "https://gitlab.com/julianfairfax/openinstallmedia"
    url "https://gitlab.com/julianfairfax/openinstallmedia/-/archive/6f8f9030e0b5c54e46e2989bb6b15075a21e26e0/openinstallmedia-6f8f9030e0b5c54e46e2989bb6b15075a21e26e0.tar.gz"
    sha256 "c05322bd54ef1214000f0b78db577805fb443275b24e68282c999c556f141621"
    license "GPL"
    version "1.1.4"
  
    def install
        bin.install "openinstallmedia.sh" => "openinstallmedia"
        bin.install "openinstallmacos.sh" => "openinstallmacos"
        bin.install "openprebootmedia.sh" => "openprebootmedia"
        bin.install "openrecoverymedia.sh" => "openrecoverymedia"
        bin.install "openupdatemacos.sh" => "openupdatemacos"
        prefix.install Dir["resources/*"]
    end
end