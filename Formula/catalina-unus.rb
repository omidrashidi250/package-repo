class CatalinaUnus < Formula
    desc "macOS command line tool for running macOS Catalina on one HFS or APFS volume"
    homepage "https://gitlab.com/julianfairfax/catalina-unus"
    url "https://gitlab.com/julianfairfax/catalina-unus/-/archive/921949024cd00379741bf1b7981bc6ecc9ce4ebb/catalina-unus-921949024cd00379741bf1b7981bc6ecc9ce4ebb.tar.gz"
    sha256 "3237467dc0db63b7f922c8daf694a181d496089f3a5ab274aa50e038ea865d03"
    license "GPL"
    version "1.2"
  
    def install
        bin.install "Catalina Unus.sh" => "catalina-unus"
        prefix.install Dir["resources/*"]
    end
end