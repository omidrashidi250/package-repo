#!/bin/bash

cd "${0%/*}"

name="gnome-dav-support"
version="1.0"
description="CardDAV, CalDAV, etc support for GNOME"

mkdir "$name"

echo "pkgname=\"$name\"
pkgver=\"$version\"
arch=(\"any\")
pkgdesc=\""$description"\"
pkgrel=\"1\"
source=(\"git+https://github.com/IceWreck/Gnome-DAV-Support-Shim\")
sha256sums=(\"SKIP\")

package() {
    cd Gnome-DAV-Support-Shim
    make build

    cp out/gnome-dav-support .

    install -d \"\${pkgdir}\"/usr/bin
    cp ./install.sh \"\${pkgdir}\"/usr/bin/install-"$name"
    cp ./"$name" \"\${pkgdir}\"/usr/bin/"$name"

    chmod +x \"\${pkgdir}\"/usr/bin/install-"$name"
    chmod +x \"\${pkgdir}\"/usr/bin/"$name"
}" | tee "$name"/PKGBUILD