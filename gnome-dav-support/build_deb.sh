#!/bin/bash

if [[ -z "$maintainer_name" ]]; then
    maintainer_name="Julian Fairfax"
fi
if [[ -z "$maintainer_email" ]]; then
    maintainer_email="juliannfairfax@protonmail.com"
fi

cd "${0%/*}"

Build(){
    git clone https://github.com/IceWreck/Gnome-DAV-Support-Shim
    cd Gnome-DAV-Support-Shim

    sed -i -z "s/build:\n	go build -o .\/out\/gnome-dav-support ./build:\n	go build -o .\/out\/gnome-dav-support .\n\nbuild-i386:\n	GOOS=linux GOARCH=386 go build -o .\/out\/gnome-dav-support .\n\nbuild-arm64:\n	GOOS=linux GOARCH=arm64 go build -o .\/out\/gnome-dav-support ./" Makefile
    sed -i -z "s/release: clean-release release-amd64 release-arm/release-i386: clean build-i386\n	mkdir --parent .\/release\n	cp .\/install.sh .\/out\n	zip --junk-paths .\/release\/gnome-dav-support-i386.zip .\/out\/*\n\nrelease-arm64: clean build-arm64\n	mkdir --parent .\/release\n	cp .\/install.sh .\/out\n	zip --junk-paths .\/release\/gnome-dav-support-arm64.zip .\/out\/*\n\nrelease: clean-release release-amd64 release-arm release-i386 release-arm64/" Makefile

    if [[ $arch == "amd64" ]]; then
        make build
    else
        make build-arm
    fi

    cp out/gnome-dav-support .

    name="gnome-dav-support"
    version="1.0"
    description="CardDAV, CalDAV, etc support for GNOME"

    mv "$name" "$name"-bin
    install -d $name/usr/bin

    cp install.sh $name/usr/bin/install-$name
    cp $name-bin $name/usr/bin/$name

    chmod +x $name/usr/bin/install-$name
    chmod +x $name/usr/bin/$name
    mkdir $name/DEBIAN

    echo "Package: $name
Version: $version
Architecture: $arch
Maintainer: $maintainer_name <$maintainer_email>
Description: $description
Section: misc
Priority: extra" | tee $name/DEBIAN/control

    dpkg-deb -Z xz -b $name/ .

    rm -r $name
    cp *.deb ../

    cd ../
    rm -rf Gnome-DAV-Support-Shim
}

export arch=amd64 && Build
export arch=armhf && Build
export arch=i386 && Build
export arch=arm64 && Build