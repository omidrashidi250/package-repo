# Package Repo

Repository for installing my projects and others more easily.

## Packages
The following projects made by me are available as packages from this repo:
- [catalina-unus](https://gitlab.com/julianfairfax/catalinua-unus) as a Homebrew package
- [macos-downloader](https://gitlab.com/julianfairfax/macos-downloader) as a Homebrew package
- [macos-patcher](https://gitlab.com/julianfairfax/macos-patcher) as a Homebrew package
- [openinstallmedia](https://gitlab.com/julianfairfax/openinstallmedia) as a Homebrew package
- [osx-patcher](https://gitlab.com/julianfairfax/osx-patcher) as a Homebrew package
- [pi-capsule-project](https://gitlab.com/julianfairfax/pi-capsule-project) as an Arch, Debian, and RPM package
- [repackage](https://gitlab.com/julianfairfax/repackage) as an Arch, Debian, and RPM package
- [spoofer](https://gitlab.com/julianfairfax/spoofer) as an Arch, Debian, RPM, and Homebrew package

In addition, the following projects made by others are available as packages from this repo:
- [etesync-web](https://github.com/etesync/etesync-web) as an Arch package, with [patches](https://gitlab.com/julianfairfax/package-repo/-/blob/master/etesync-web/build_arch.sh#L23-25) to support http servers
- [gnome-dav-support](https://github.com/IceWreck/Gnome-DAV-Support-Shim) as an Arch and Debian package, with [instructions](#how-to-use-gnome-dav-support) on how to use it
- [hydroxide](https://github.com/emersion/hydroxide) as a Debian package

You can also build these packages yourself by running the build scripts.

## How to add repository for Arch-based Linux distributions
```
echo "[package-repo]
SigLevel = Optional TrustAll
Server = https://julianfairfax.gitlab.io/package-repo/" | sudo tee -a /etc/pacman.conf

wget https://gitlab.com/julianfairfax/package-repo/raw/master/pub.gpg -O - | sudo pacman-key --add -
sudo pacman-key --lsign-key C123CB2B21B9F68C80A03AE005B2039A85E7C70A
```

## How to add repository for Debian-based Linux distributions
```
wget -qO - https://gitlab.com/julianfairfax/package-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/usr/share/keyrings/julian-fairfax-package-repo-archive-keyring.gpg

echo 'deb [ signed-by=/usr/share/keyrings/package-repo-archive-keyring.gpg ] https://julianfairfax.gitlab.io/package-repo/debs packages main' | sudo tee /etc/apt/sources.list.d/julian-fairfax-package-repo.list
```

## How to add repository for Homebrew
```
brew tap julianfairfax/package-repo https://gitlab.com/julianfairfax/package-repo
```

## How to add repository for RPM-based Linux distributions
```
echo "[gitlab.com.julianfairfax_package_repo]
name=gitlab.com_julianfairfax_package_repo
baseurl=https://julianfairfax.gitlab.io/package-repo/rpms/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/julianfairfax/package-repo/raw/master/pub.gpg
metadata_expire=1h" | sudo tee -a /etc/yum.repos.d/julian-fairfax-package-repo.repo
```

## How to use gnome-dav-support
Simply run the install script, which sets up a systemd service that starts when you log in:

```
install-gnome-dav-support --cal "${caldav-service-url}" --card "${carddav-service-url}"
```